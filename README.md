Program do testowania studentów.

Klasa uruchamiająca program -- App.java

Hasło: password12345


Spełnione wymagania funckjonalne:
1. Dwa tryby działania: studencki i administratorski
2. Logowanie do administratorskiego trybu przez hasło
3. Tworzenie testów w trybie administratorskim
4. Możliwość tworzenia pytań wielokrotnego wyboru
5. Opcja punktów ujemnych dla błędnych odpowiedzi
6. Wyświetlenie wyników po zakończeniu testowania


Spełnione wymagania niefunkcjonalne:
1. Modyfikowanie testów jest niemożliwe bez hasła.
2. Hasło admnistratorskie do programu jest przechowywane w postaci haszu.

