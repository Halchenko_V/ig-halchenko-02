package com.mycompany.quiz;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import javax.xml.bind.DatatypeConverter;
/**
 *
 * @author vhhl
 */
public class Access {
    private final String algo;
    private final String path;
    
    public Access(String algo, String keyPath) {
        this.algo = algo;
        this.path = keyPath;
    }
    
    public boolean access(String password) throws NoSuchAlgorithmException, FileNotFoundException, IOException {
        MessageDigest md = MessageDigest.getInstance(this.algo);
        md.update(password.getBytes());

        byte[] digest = md.digest();
        String myHash = DatatypeConverter.printHexBinary(digest).toLowerCase();
        
        
        StringBuilder sb;
        try (Scanner in = new Scanner(new FileReader(this.path))) {
            sb = new StringBuilder();
            while(in.hasNext()) {
                sb.append(in.next());
            }
        }
        String fileKeyHash = sb.toString().trim().toLowerCase();
        System.out.println(fileKeyHash);        
        System.out.println(myHash);
        return (fileKeyHash == null ? myHash == null : fileKeyHash.equals(myHash));
    }
    
}
